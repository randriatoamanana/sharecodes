import yaml
import types
import datetime

class Glob(object):
  """Espace de noms pour les variables et fonctions <pseudo-globales>"""
  print("hello")
  dbName = "testpython"      # nom de la base de données, anciennement "2018-projet-22"
  user = "dimitri3502"	  # propriétaire ou utilisateur, anciennement "postgres"
  passwd = "25991432"	    # mot de passe d'accès
  host = "localhost"	      # nom ou adresse IP du serveur
  port =5432
 
  # Structure de la base de données.  Dictionnaire des tables & champs :

  dicoT ={
  "utilisateur":[
      ('id_util', "k", "clé primaire"),
      ('nom', 25, "nom"),
      ('prenom', 25, "prénom"),
      ('a_naiss', "i", "année de naissance"),
      ('date_fin', "date", "date de fin d'utilisation"),
      ('id_chefProjet', 30, "id chef de projet"),],
  "projet":[
      ('id_projet', "k", "clé primaire"),
	    ('nom_projet', 50, "nom du projet"),
	    ('id_chefProjet', 30, "id du chef de projet"),],
  "projetAfecteA":[
      ('id_projetAfecteA', "k", "clé primaire"),
      ('id_projet', "i", "clé projet"),
      ('id_util', "i", "clé utilisateur")
      ]}

def Yaml2Table(in_file):
    "procédure d'entrée d'un fichier Yaml"
    in_file  = open(in_file, "r")
    userRecord=[]
    
    
    with in_file as stream: 
      data_loaded = yaml.load(stream) # chargement des données yaml
      for cle,valeur in data_loaded.items():
        userRecord.append(cle)
        nomTables=[]
        for cle,valeur in valeur.items():
          nomTables.append(cle)
          nomChamp=[]
          valeurChamp=[]
          for cle,valeur in valeur.items():
            nomChamp.append(cle)

            if isinstance(valeur, int) :
              valeurChamp.append(type(valeur))

    print(nomTables,nomChamp,valeurChamp)
# Yaml2Table('file.yaml')



