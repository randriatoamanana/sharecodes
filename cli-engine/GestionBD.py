import sys
import psycopg2
from Glob import *
import types
import datetime

#dimitri
class GestionBD(object):
	"""Mise en place et interfaçage d'une base de données PostgreSQL"""
	def __init__(self, dbName, user, passwd, host, port =5432):
		"Établissement de la connexion - Création du curseur"
		try:
			self.baseDonn = psycopg2.connect(host =host, port =port,
				database =dbName,
				user=user, password=passwd)
			self.dbName=dbName
		except Exception as err:
			print('La connexion avec la base de données a échoué :\n'\
				'Erreur détectée :\n%s' % err)
			self.echec =1
		else:
			self.cursor = self.baseDonn.cursor()   # création du curseur
			self.echec =0

	def creerTablesYaml(self, in_file):
		"Création des tables décrites dans le fichier Yaml."
		nbCharMax=50 # nombre max de caracteres dans un champs de type varchar
		in_file  = open(in_file, "r")
		userRecord=[]
		alterList = []
		with in_file as stream: 
			data_loaded = yaml.load(stream) # chargement des données yaml

			for cle,valeur in data_loaded.items():
				userRecord.append(cle)
				for nomTable,valeur in valeur.items():
					req = "CREATE TABLE %s (" % nomTable
					req = req + "%s %s, " % ("id_%s" % nomTable, 'SERIAL') # champ 'clé primaire' (entier incrémenté automatiquement)
					pk ="id_%s" % nomTable # champs clé primaire créé automatiquement en fonction du nom de la table
					
					for nomChamp,valeurChamp in valeur.items():
						if isinstance(valeurChamp, int) :
							typeChamp ='INTEGER'
						elif isinstance(valeurChamp, datetime.date) :
					    	# champ 'date'
							typeChamp ='date'
						else:
							typeChamp ="VARCHAR(%s)" % nbCharMax
						req = req + "%s %s, " % (nomChamp, typeChamp)
					req = req + "CONSTRAINT %s_pk PRIMARY KEY(%s))" % (pk, pk)
					self.executerReq(req) 

	def addForeignKey(self,nomTable, nomChamp, nomTableReference, nomChampReference):
		req  = "ALTER TABLE %s ADD CONSTRAINT %s FOREIGN KEY (%s) REFERENCES %s (%s) MATCH FULL;" % (nomTable, nomChamp, nomChamp, nomTableReference, nomChampReference)
		self.executerReq(req) 
		print(req)

	def addYaml(self, in_file):
		in_file1  = open(in_file, "r")
		with in_file1 as stream: 
			data_loaded = yaml.load(stream) # chargement des données yaml
			for cle,valeur in data_loaded.items():
				for nomTable,Table in valeur.items():
					champs ="("  # ébauche de chaîne pour les noms de champs
					valeurs =[]  # liste pour les valeurs correspondantes
					for nomChamp,valeurChamp in Table.items():
						if nomChamp.endswith('_fk'):  # les fk sont insérés ensuite
							continue  # à l'utilisateur (numérotation auto.)
						champs = champs + nomChamp + ","
						valeurs.append(valeurChamp)

					balises ="(" + "%s," * len(valeurs)     # balises de conversion
					champs = champs[:-1] + ")"    # supprimer la dernière virgule,
					balises = balises[:-1] + ")"  # et ajouter une parenthèse
					req ="INSERT INTO %s %s VALUES %s" % (nomTable, champs, balises)
					self.executerReq(req, valeurs)
		in_file1.close()


	def supprimerTable(self, nomTable):
		"Suppression de toutes les tables décrites dans <dicTables>"
		req ="DROP TABLE %s" % nomTable
		self.executerReq(req)
		self.commit()    # transfert -> disque

	def supprimerToutesTable(self):
		"Suppression de toutes les tables décrites dans la bdd"
		req = "drop owned by %s" % Glob.user
		self.executerReq(req)
		self.commit()    # transfert -> disque

	def executerReq(self, req, param =None):
		"Exécution de la requête <req>, avec détection d'erreur éventuelle"
		try:
			self.cursor.execute(req, param)
		except Exception as err:
			# afficher la requête et le message d'erreur système :
			print("Requête SQL incorrecte :\n{}\nErreur détectée :".format(req))
			print(err)
			return 0
		else:
			return 1

	def resultatReq(self):
		"renvoie le résultat de la requête précédente (une liste de tuples)"
		return self.cursor.fetchall()

	def commit(self):
		if self.baseDonn:
			self.baseDonn.commit()	 # transfert curseur -> disque

	def close(self):
		if self.baseDonn:
			self.baseDonn.close()