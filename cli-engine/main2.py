###### Programme principal : #########
from GestionBD import *
from Glob import *

# auteur dimitri
import sys
import os
# Création de l'objet-interface avec la base de données :
bd = GestionBD(Glob.dbName, Glob.user, Glob.passwd, Glob.host, Glob.port)
if bd.echec:
  sys.exit()
 
if sys.argv[1] in ["-help","-h"]:

  print("\nCOMMANDES DISPONIBLES :\n"\
     "-createTableYaml yaml_file      Crée les tables de la base de données a partir d'un fichier Yaml\n"\
     "-deleteTable nomTable           Supprime la table nomTable de la base de données \n"\
     "-deleteAllTable                 Supprime la table nomTable de la base de données \n"\
     "-execute requêteSQL             Exécute une requête SQL quelconque\n"\
     "-addYaml yaml_file              ajoute des données du yaml_file dans la bdd\n"\
     "                                --> le fichier yaml est dans le répertoire courant\n")

elif sys.argv[1] =="-createTableYaml":
    # création de toutes les tables décrites dans le dictionnaire :
    yaml_file = sys.argv[2]
    bd.creerTablesYaml(yaml_file)

elif sys.argv[1] =="-deleteTable":
    # suppression de toutes les tables décrites dans le dic. :
    nomTable=sys.argv[2]
    bd.supprimerTable(nomTable)

elif sys.argv[1] =="-deleteAllTable":
    # suppression de toutes les tables décrites dans le dic. :
    bd.supprimerToutesTable()

elif sys.argv[1] =="-execute":
    print("Requête SQL : "+sys.argv[2])
    if bd.executerReq(sys.argv[2]):
      print(bd.resultatReq())	    # ce sera une liste de tuples

elif sys.argv[1] =="-addYaml":
    
    in_file = sys.argv[2]
    "procédure d'entrée d'un fichier Yaml"
    bd.addYaml(in_file)

else:
  print("Commande non prise en charge")

bd.commit()
bd.close()
    